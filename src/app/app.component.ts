import { Component } from '@angular/core';

@Component({
  selector: 'tie-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tinity-education-webclient';
}
